from wtforms.validators import Email

from api import db, flask_bcrypt


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120),
                      unique=True,
                      nullable=False,
                      info={'validators': Email()})
    password = db.Column(db.String(80), nullable=False)
    payment_id = db.Column(db.String(255))

    maps = db.relationship('Map', backref='user')

    def __init__(self, email, password):
        self.email = email
        self.password = flask_bcrypt.generate_password_hash(password)

    def __repr__(self):
        return '<User %r>' % self.email


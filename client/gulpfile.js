var pkg = require('./package.json'),
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    source = require('vinyl-source-stream'),
    connect = require('gulp-connect'),
    sftp = require('gulp-sftp'),
    templateCache = require('gulp-angular-templatecache'),
    livereload = require('gulp-livereload');

gulp.task('js', function(){

  gulp.src(['./src/javascript/config.js',
           './src/javascript/application.js'])
    .pipe(sourcemaps.init())
    .pipe(concat('app.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/js'))
    .pipe(livereload());

  gulp.src(pkg.bowerFiles.javascript)
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('./dist/js'))
    .pipe(livereload());

});

gulp.task('templates', function(){

  gulp.src('./src/templates/**/*.html')
    .pipe(templateCache('templates.js', {
      standalone: true
    }))
    .pipe(gulp.dest('./dist/js'))
    .pipe(livereload());

})

gulp.task('css', function(){

  // Application CSS Files
  gulp.src('./src/stylesheets/*.scss')
    .pipe(sourcemaps.init())
      .pipe(sass({ style: 'compressed' }))
    .pipe(concat('app.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/css/'))
    .pipe(livereload());

  // Vendor CSS Files
  gulp.src(pkg.bowerFiles.stylesheets)
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest('./dist/css/'))
    .pipe(livereload());

})

gulp.task('sync', function () {

  gulp.src('./src/*.html')
    .pipe(gulp.dest('./dist'))
    .pipe(livereload());

  gulp.src('./src/images/**/*.*')
    .pipe(gulp.dest('./dist/images'))
    .pipe(livereload());

  gulp.src('./bower_components/font-awesome/fonts/**/*.*')
    .pipe(gulp.dest('./dist/fonts'))
    .pipe(livereload());

})

gulp.task('connect', function() {

  connect.server({
    root: './dist',
    // livereload: true,
    port: 8000
  });
});

gulp.task('reload', function () {
  connect.reload();
})

gulp.task('sftp_staging', function () {
  gulp.src('./dist/**/*')
    .pipe(sftp({
      host: pkg.staging.host,
      remotePath: pkg.staging.remote_path,
      auth: 'development',
      buffer:false
    }))
})
gulp.task('sftp_production', function () {
  gulp.src('./dist/*')
    .pipe(sftp({
      host: pkg.production.host,
      remotePath: pkg.production.remote_path
    }))
})

gulp.task('watch', function () {
  livereload.listen();
  gulp.watch(['./src/javascript/**/*.js'], ['js']);
  gulp.watch(['./src/templates/**/*.html'], ['templates'])
  gulp.watch(['./src/stylesheets/**/*.scss'], ['css']);
  gulp.watch(['./src/*.html',
              './src/images/**/*.*'], ['sync']);
});

gulp.task('default', ['js', 'templates', 'css', 'sync', 'connect', 'watch']);
gulp.task('stage', ['js', 'templates', 'css', 'sync', 'sftp_staging']);
gulp.task('deploy', ['js', 'css', 'sync', 'sftp_production']);

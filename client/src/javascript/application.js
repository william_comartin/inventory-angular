angular.module('app', ['ui.bootstrap', 'dialogs.main', 'templates'])
  .config([function(){

  }])
  .run(['$rootScope', function($rootScope){

    var templateProduct = {
      product_number: "DSA-2134",
      product_name: "Spark Plug",
      price: 54.23,
      make: "Briggs & Stratton",
      model: "Product Model",
      tags: [
        "spark plug",
        "0.7L"
      ]
    }

    $rootScope.edit = false;

    $rootScope.inventory = []

    for(var i = 0; i < 100; i++){
      $rootScope.inventory.push(angular.extend({}, templateProduct, {
        product_number: templateProduct.product_number + "-" + i,
        tags: angular.copy(templateProduct.tags)
      }));
    }

  }])
  .directive('inventory', ['$modal', function($modal){
    return {
      restrict: "A",
      scope: '=',
      templateUrl: 'inventory.html',
      link: function(scope, element, attrs){

        scope.edit = function(){
          scope.$parent.edit = true;
          scope.$parent.current_item = scope.item;
        }

      }
    }
  }])
  .directive('product', ['$modal', function($modal){
    return {
      restrict: 'E',
      scope: {
        item: '='
      },
      templateUrl: 'product.html',
      link: function(scope, element, attrs){

        scope.remove = function(tag){
          scope.item.tags.splice(scope.item.tags.indexOf(tag), 1);
        }

        scope.addTag = function(){
          $modal.open({
            templateUrl: 'addTag.html',
            controller: ['$scope', '$modalInstance', function($scope, $modalInstance){
              $scope.save = function(){
                console.log($scope)
                scope.item.tags.push($scope.tag_input);
                $modalInstance.close();
              }
              $scope.cancel = function(){
                $modalInstance.dismiss('cancel');
              }
            }]
          })
        }

      }
    }
  }])
  .factory('customSearch', ['$rootScope', function($rootScope){
    return {
      checkSearch: function(item){

        var flag = false;
        cs = $rootScope.customSearch

        if(typeof(cs) == 'undefined' || cs == '') return true;

        var searches = cs.split(" ");

        angular.forEach(searches, function(s){

          if(item.product_number.indexOf(s) !== -1) flag = true;
          if(item.product_name.indexOf(s) !== -1) flag = true;
          if(item.make.indexOf(s) !== -1) flag = true;
          if(item.model.indexOf(s) !== -1) flag = true;


          angular.forEach(item.tags, function(tag){
            if(tag.indexOf(s) !== -1){
              flag = true;
            }
          })
        })

        return flag
      }
    }
  }])
  .filter('customSearch', ['customSearch', function(customSearch){
    return function(items){
      var filtered = [];
      angular.forEach(items, function(item){
        if(customSearch.checkSearch(item)){
          filtered.push(item);
        }
      })
      return filtered
    }
  }])